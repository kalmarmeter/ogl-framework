#include "program.h"

#include <iostream>

using namespace gl;

Program::Program() {
	create();
}

Program::Program(const VertexShader& vs, const FragmentShader& fs) {
	create();
	attachShader(vs);
	attachShader(fs);
}

Program::~Program() {
	clean();
}

void Program::bindAttribLocation(GLuint index, const std::string& name) {
	glBindAttribLocation(id_, index, name.c_str());
}

bool Program::link() {
	if (id_ == 0) {
		return false;
	}
	
	glLinkProgram(id_);

	GLint result = GL_FALSE;
	GLint errorLength;

	glGetProgramiv(id_, GL_LINK_STATUS, &result);
	glGetProgramiv(id_, GL_INFO_LOG_LENGTH, &errorLength);

	if (result == GL_FALSE) {
		GLchar* errormsg = new GLchar[errorLength];
		glGetProgramInfoLog(id_, errorLength, nullptr, errormsg);
		std::cerr << "Program linking error: " << errormsg << std::endl;
		delete[] errormsg;

		return false;
	}

	return true;
}

void Program::use() {
	glUseProgram(id_);
}

void Program::create() {
	id_ = glCreateProgram();
}

void Program::clean() {
	for (GLuint shader : attachedShaders_) {
		glDetachShader(id_, shader);
	}

	if (id_ != 0) {
		glDeleteProgram(id_);
	}
}

GLuint Program::findUniformLocation(const std::string& name) {
	auto it = uniformLocations_.find(name);
	if (it == uniformLocations_.end()) {
		GLuint loc = glGetUniformLocation(id_, name.c_str());
		uniformLocations_[name] = loc;
		return loc;
	}
	else {
		return it->second;
	}
}

void Program::setUniform1i(const std::string& name, GLint value) {
	glUniform1i(findUniformLocation(name), value);
}

void Program::setUniform2i(const std::string& name, GLint value1, GLint value2) {
	glUniform2i(findUniformLocation(name), value1, value2);
}

void Program::setUniform3i(const std::string& name, GLint value1, GLint value2, GLint value3) {
	glUniform3i(findUniformLocation(name), value1, value2, value3);
}

void Program::setUniform4i(const std::string& name, GLint value1, GLint value2, GLint value3, GLint value4) {
	glUniform4i(findUniformLocation(name), value1, value2, value3, value4);
}

void Program::setUniform1f(const std::string& name, GLfloat value) {
	glUniform1f(findUniformLocation(name), value);
}

void Program::setUniform2f(const std::string& name, GLfloat value1, GLfloat value2) {
	glUniform2f(findUniformLocation(name), value1, value2);
}

void Program::setUniform3f(const std::string& name, GLfloat value1, GLfloat value2, GLfloat value3) {
	glUniform3f(findUniformLocation(name), value1, value2, value3);
}

void Program::setUniform4f(const std::string& name, GLfloat value1, GLfloat value2, GLfloat value3, GLfloat value4) {
	glUniform4f(findUniformLocation(name), value1, value2, value3, value4);
}

void Program::setUniformMat2(const std::string& name, const GLfloat* matrix) {
	glUniformMatrix2fv(findUniformLocation(name), 1, GL_FALSE, matrix);
}

void Program::setUniformMat3(const std::string& name, const GLfloat* matrix) {
	glUniformMatrix3fv(findUniformLocation(name), 1, GL_FALSE, matrix);
}

void Program::setUniformMat4(const std::string& name, const GLfloat* matrix) {
	glUniformMatrix4fv(findUniformLocation(name), 1, GL_FALSE, matrix);
}