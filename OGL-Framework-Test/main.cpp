#include <window.h>

#include "testapp.h"

#undef main

int main() {
	gl::Window window("Test test test", 640, 480, false);
	TestApp app;
	window.setApp(&app);

	app.init();

	window.loop();


}