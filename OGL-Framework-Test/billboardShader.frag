#version 440

in vec2 vs_out_tex;

out vec4 fs_out_col;

uniform sampler2D grass_texture;

void main() {
	vec4 col = texture(grass_texture, vs_out_tex);
	if (col.a < 1.0) discard;
	
	fs_out_col = col;
}