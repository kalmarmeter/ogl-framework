#include "window.h"

using namespace gl;

Window::Window(const std::string& title, int width, int height, bool resizable) {
	initSDL();
	setGLAttributes();
	initWindow(title, width, height);
	setResizable(resizable);
	initOGLContext();
	checkOGLVersion();
	initGLEW();
}

void Window::setResizable(bool resizable) {
	SDL_SetWindowResizable(window_, resizable ? SDL_TRUE : SDL_FALSE);
}

void Window::setApp(App* app) {
	app_ = app;
}

void Window::loop() {

	if (!app_) {
		throw WindowException(WindowExceptionType::NO_APP_ERROR);
	}

	bool active = true;
	while (active) {

		frameStart = SDL_GetTicks();

		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			app_->processEvent(event);

			switch (event.type) {
			case SDL_QUIT:
				active = false;
				break;
			default:
				break;
			}
		}

		app_->update(frameTime);

		glClear(GL_COLOR_BUFFER_BIT);

		app_->draw();

		SDL_GL_SwapWindow(window_);

		frameTime = SDL_GetTicks() - frameStart;
	}

}

Window::~Window() {
	SDL_Quit();
	SDL_GL_DeleteContext(context_);
	SDL_DestroyWindow(window_);
}

void Window::exitCallback() {
	//empty
}

void Window::initSDL() {
	if (SDL_Init(SDL_INIT_VIDEO) == -1) {
		throw WindowException(WindowExceptionType::SDL_INIT_ERROR);
	}
}

void Window::setGLAttributes() {
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
}

void Window::initWindow(const std::string& title, int width, int height) {
	window_ = SDL_CreateWindow(title.c_str(),
		200,
		200,
		width,
		height,
		SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);

	if (!window_) {
		throw WindowException(WindowExceptionType::SDL_WINDOW_ERROR);
	}

	SDL_GL_SetSwapInterval(1);
}

void Window::initOGLContext() {
	context_ = SDL_GL_CreateContext(window_);

	if (context_ == NULL) {
		throw WindowException(WindowExceptionType::OGL_CONTEXT_ERROR);
	}
}

void Window::initGLEW() {
	GLenum errorCode;
	errorCode = glewInit();

	if (errorCode != GLEW_OK) {
		throw WindowException(WindowExceptionType::GLEW_INIT_ERROR);
	}
}

void Window::checkOGLVersion() {
	GLint versionMajor = -1, versionMinor = -1;
	glGetIntegerv(GL_MAJOR_VERSION, &versionMajor);
	glGetIntegerv(GL_MINOR_VERSION, &versionMinor);
	if (versionMajor == -1 || versionMinor == -1) {
		throw WindowException(WindowExceptionType::OGL_VERSION_ERROR);
	}
}