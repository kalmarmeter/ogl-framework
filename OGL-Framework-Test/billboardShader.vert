#version 440

in vec3 vs_in_pos;
in vec3 vs_in_norm;

out vec2 vs_out_tex;

uniform vec3 billboard_pos;
uniform mat4 VP;
uniform vec3 cam_right;
uniform vec3 cam_up;
uniform sampler2D grass_texture;

void main() {
	
	vec3 vertex_world_position = billboard_pos +
			cam_right * vs_in_pos.x * 1.3f +
			cam_up * vs_in_pos.y * 1.3f;
			
	vs_out_tex = vs_in_pos.xy + vec2(0.5, 0.0);
			
	gl_Position = VP * vec4(vertex_world_position, 1.0);
	
}