#pragma once

#include <GL/glew.h>
#include <SDL.h>

class App {
public:
	virtual void init() = 0;
	virtual void processEvent(SDL_Event& event) = 0;
	virtual void update(Uint32 ms) = 0;
	virtual void draw() = 0;
};

