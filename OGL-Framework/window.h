#pragma once

#include <exception>
#include <string>

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>

#include "app.h"

namespace gl {

	class Window {
	public:
		Window(const std::string& title, int width, int height, bool resizable);
		void setResizable(bool resizable);
		void setApp(App* app);
		void loop();
		~Window();
	private:
		SDL_Window* window_;
		SDL_GLContext context_;
		App* app_;
		Uint32 frameStart = 0;
		Uint32 frameTime = 0;

		void exitCallback();
		void initSDL();
		void setGLAttributes();
		void initWindow(const std::string& title, int width, int height);
		void initOGLContext();
		void initGLEW();
		void checkOGLVersion();
	};

	enum class WindowExceptionType {
		SDL_INIT_ERROR,
		SDL_WINDOW_ERROR,
		OGL_CONTEXT_ERROR,
		GLEW_INIT_ERROR,
		OGL_VERSION_ERROR,
		NO_APP_ERROR
	};

	class WindowException : public std::exception {
	public:
		WindowException(WindowExceptionType type) : type_(type) {}
		const char* what() const override {
			switch (type_) {
			case WindowExceptionType::SDL_INIT_ERROR:
				return "[WINDOW] SDL initialization error\n";
				break;
			case WindowExceptionType::SDL_WINDOW_ERROR:
				return "[WINDOW] SDL window creation error\n";
				break;
			case WindowExceptionType::OGL_CONTEXT_ERROR:
				return "[WINDOW] OGL context creation error\n";
				break;
			case WindowExceptionType::GLEW_INIT_ERROR:
				return "[WINDOW] GLEW initialization error\n";
				break;
			case WindowExceptionType::OGL_VERSION_ERROR:
				return "[WINDOW] OGL version check failed\n";
				break;
			case WindowExceptionType::NO_APP_ERROR:
				return "[WINDOW] App is not set before calling run\n";
				break;
			default:
				return "[WINDOW] Unknown exception\n";
				break;
			}
		}
	private:
		WindowExceptionType type_;
	};

}