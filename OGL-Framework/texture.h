#pragma once

#include <string>

#include <GL/glew.h>
#include <SDL_image.h>

namespace gl {

	class Texture {
	public:
		Texture();
		Texture(const Texture&) = delete;
		Texture& operator=(const Texture&) = delete;
		~Texture();

		bool loadFromFile(const std::string& filename);

		void bind();
		void unbind();

		GLuint sampler() const;

	private:
		GLuint id_ = 0;
		GLuint sampler_ = 0;

		void create();
		void clean();

		bool invertSurface(SDL_Surface*& surface);
	};

}