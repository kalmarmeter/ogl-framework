#version 440

in vec3 vs_in_pos;
in vec3 vs_in_norm;
in vec2 vs_in_tex;

void main() {
	gl_Position = vec4(vs_in_pos, 1);
}