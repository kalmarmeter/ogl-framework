#include "vao.h"

using namespace gl;

VAO::VAO() {
	create();
}

void VAO::bind() {
	glBindVertexArray(id_);
}

void gl::VAO::addAttribute(VBO& vbo, GLuint index, GLint count, GLenum type, GLboolean normalized, GLsizei stride, const void* ptr) {
	bind();
	vbo.bind();
	glEnableVertexAttribArray(index);
	glVertexAttribPointer(index, count, type, normalized, stride, ptr);
	vbo.unbind();
	unbind();
}

void VAO::unbind() {
	glBindVertexArray(0);
}

VAO::~VAO() {
	clean();
}

void VAO::create() {
	glGenVertexArrays(1, &id_);
}

void VAO::clean() {
	if (id_ != 0) {
		glDeleteVertexArrays(1, &id_);
	}
}
