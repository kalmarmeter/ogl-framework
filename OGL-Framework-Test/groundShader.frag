#version 440

in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex;

out vec4 fs_out_col;

uniform sampler2D groundTexture;

uniform vec3 lightDir = vec3(1.0, -2.0, 2.0);
uniform vec3 lightColor = vec3(1.0, 1.0, 1.0);
uniform vec3 ambientColor = vec3(0.2, 0.5, 0.1);

void main() {
	
	vec3 L = normalize(-lightDir);
	vec3 N = normalize(vs_out_norm);

	vec3 texColor = texture(groundTexture, vs_out_tex).xyz;
	
	float diff = clamp(dot(L, N), 0, 1);
	
	fs_out_col = vec4(texColor * (ambientColor + diff * lightColor), 1);
}