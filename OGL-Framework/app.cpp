/*
#include "app.h"

#include <glm/glm.hpp>

App::App() {
	init();
}

App::~App() {
	
}

void App::init() {

	std::vector<glm::vec3> vertices = {
		{-1.0f, -1.0f, 0.0f},
		{1.0f, -1.0f, 0.0f},
		{1.0f, 1.0f, 0.0f},
		{-1.0f, 1.0f, 0.0f}
	};

	vbo.bufferData(vertices);

	std::vector<GLuint> indices = {
		0, 1, 3,
		3, 1, 2
	};

	ib.bufferData(indices);

	vao.bind();
	vao.addAttribute(vbo, 0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	vao.unbind();

	vs.loadFromFile("test.vert");
	fs.loadFromFile("test.frag");

	shader.attachShader(vs);
	shader.attachShader(fs);
	shader.link();

	shader.bindAttribLocation(0, "vs_in_pos");

	texture.loadFromFile("test.bmp");

}

void App::processEvent(SDL_Event& event) {
}

void App::update() {
}

void App::draw() {

	shader.use();
	shader.setUniform1i("sampler", 0);

	vao.bind();
	texture.bind();
	ib.bind();

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);

	ib.unbind();
	texture.unbind();
	vao.unbind();

}
*/