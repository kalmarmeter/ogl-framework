#pragma once

#include <vector>

#include <GL/glew.h>

namespace gl {

	enum BufferType {
		ArrayBuffer = GL_ARRAY_BUFFER,
		IndexBuffer = GL_ELEMENT_ARRAY_BUFFER
	};

	template<BufferType Type>
	class Buffer {
	public:
		Buffer();
		Buffer(const Buffer&) = delete;
		Buffer& operator=(const Buffer&) = delete;

		template<typename T>
		void bufferData(const std::vector<T>& vec);
		
		void bind();
		void unbind();

		~Buffer();

	private:
		GLuint id_ = 0;
		GLuint size_ = 0;

		void create();
		void clean();
	};

	using VBO = Buffer<ArrayBuffer>;
	using IB = Buffer<IndexBuffer>;

	//------------------------------------------------

	template<BufferType Type>
	inline Buffer<Type>::Buffer() {
		create();
	}

	template<BufferType Type>
	inline Buffer<Type>::~Buffer() {
		clean();
	}

	template<BufferType Type>
	inline void Buffer<Type>::create() {
		glGenBuffers(1, &id_);
	}

	template<BufferType Type>
	inline void Buffer<Type>::bind() {
		glBindBuffer(static_cast<GLenum>(Type), id_);
	}

	template<BufferType Type>
	inline void Buffer<Type>::unbind() {
		glBindBuffer(static_cast<GLenum>(Type), 0);
	}

	template<BufferType Type>
	inline void Buffer<Type>::clean() {
		if (id_ != 0) {
			glDeleteBuffers(1, &id_);
		}
	}


	template<BufferType Type>
	template<typename T>
	inline void Buffer<Type>::bufferData(const std::vector<T>& vec) {
		bind();
		size_ = vec.size() * sizeof(T);
		glBufferData(static_cast<GLenum>(Type), size_, vec.data(), GL_STATIC_DRAW);
		unbind();
	}

}
