#include "texture.h"

#include <iostream>

using namespace gl;

Texture::Texture() {
	create();
}

Texture::~Texture() {
	clean();
}

void Texture::create() {
	glGenTextures(1, &id_);
	glGenSamplers(1, &sampler_);
	glSamplerParameteri(sampler_, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glSamplerParameteri(sampler_, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
}

void Texture::clean() {
	if (id_ != 0) {
		glDeleteTextures(1, &id_);
	}

	if (sampler_ != 0) {
		glDeleteSamplers(1, &sampler_);
	}
}

void Texture::bind() {
	glBindTexture(GL_TEXTURE_2D, id_);
	glBindSampler(0, sampler_);
}

void Texture::unbind() {
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindSampler(0, 0);
}

GLuint Texture::sampler() const {
	return sampler_;
}

bool Texture::loadFromFile(const std::string& filename) {
	SDL_Surface* surface = IMG_Load(filename.c_str());
	if (!surface) {
		std::cerr << "Texture load error" << std::endl;
		return false;
	}

	Uint32 format;
	if (SDL_BYTEORDER == SDL_LIL_ENDIAN) {
		format = SDL_PIXELFORMAT_ABGR8888;
	}
	else {
		format = SDL_PIXELFORMAT_RGBA8888;
	}

	SDL_Surface* formatted = SDL_ConvertSurfaceFormat(surface, format, 0);
	if (!formatted) {
		std::cerr << "Texture formatting error" << std::endl;
		return false;
	}

	invertSurface(formatted);

	bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, formatted->w, formatted->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, formatted->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	unbind();

	SDL_FreeSurface(surface);
	SDL_FreeSurface(formatted);

	return true;

}

bool Texture::invertSurface(SDL_Surface*& surface) {
	SDL_Surface* flipped = SDL_CreateRGBSurface(
		surface->flags,
		surface->w, surface->h,
		surface->format->BytesPerPixel * 8,
		surface->format->Rmask,
		surface->format->Gmask,
		surface->format->Bmask,
		surface->format->Amask);

	if (!flipped) {
		return false;
	}

	for (int y = 0; y < surface->h; ++y) {
		//Let's assume the pixels are 32 bit colors
		Uint32* srcBegin = (Uint32*)(surface->pixels) + y * surface->w;
		Uint32* srcEnd = (Uint32*)(surface->pixels) + (y + 1) * surface->w;
		Uint32* dstBegin = (Uint32*)(flipped->pixels) + (flipped->h - y - 1) * flipped->w;
		std::copy(srcBegin, srcEnd, dstBegin);
	}

	SDL_FreeSurface(surface);
	surface = flipped;

	return true;
}