#include "testapp.h"

void TestApp::getGroundVertices(std::vector<Vertex>& groundVertices_) {

	groundVertices_.resize(WIDTH * HEIGHT);
	//Get positions and texture coordinates
	for (int x = 0; x < WIDTH; ++x) {
		for (int z = 0; z < HEIGHT; ++z) {
			float noiseValue = noise_.GetPerlin(x * 10.0f, z * 10.0f);
			groundVertices_[x * HEIGHT + z].pos = glm::vec3(x, noiseValue * 10.0f, z);
			//The texture is tiled every 16 units
			groundVertices_[x * HEIGHT + z].tex = glm::vec2(x / 16.0f, z / 16.0f);
		}
	}

	//Get normals by averaging the neighbours
	for (int x = 0; x < WIDTH; ++x) {
		for (int z = 0; z < HEIGHT; ++z) {
			glm::vec3 curr = groundVertices_[x * HEIGHT + z].pos;
			//Get vectors looking towards neighbors (if they exist)
			glm::vec3 top = ((z < HEIGHT - 1) ? groundVertices_[x * HEIGHT + (z + 1)].pos : curr) - curr;
			glm::vec3 bottom = ((z > 0) ? groundVertices_[x * HEIGHT + (z - 1)].pos : curr) - curr;
			glm::vec3 left = ((x > 0) ? groundVertices_[(x - 1) * HEIGHT + z].pos : curr) - curr;
			glm::vec3 right = ((x < WIDTH - 1) ? groundVertices_[(x + 1) * HEIGHT + z].pos : curr) - curr;
			//Normal is the average of the cross product of these
			glm::vec3 normal = glm::cross(left, top) + glm::cross(top, right) + glm::cross(right, bottom) + glm::cross(bottom, left);
			normal /= 4.0f;

			groundVertices_[x * HEIGHT + z].normal = normal;
		}
	}

}

void TestApp::getGroundIndices(std::vector<GLushort>& groundIndices_) {

	groundIndices_.resize((WIDTH - 1) * (HEIGHT - 1) * 6);

	for (int x = 0; x < WIDTH - 1; ++x) {
		for (int z = 0; z < HEIGHT - 1; ++z) {
			groundIndices_[(z + x * (HEIGHT - 1)) * 6 + 0] = x + z * WIDTH;
			groundIndices_[(z + x * (HEIGHT - 1)) * 6 + 1] = x + (z + 1) * WIDTH;
			groundIndices_[(z + x * (HEIGHT - 1)) * 6 + 2] = (x + 1) + z * WIDTH;
			groundIndices_[(z + x * (HEIGHT - 1)) * 6 + 3] = (x + 1) + z * WIDTH;
			groundIndices_[(z + x * (HEIGHT - 1)) * 6 + 4] = x + (z + 1) * WIDTH;
			groundIndices_[(z + x * (HEIGHT - 1)) * 6 + 5] = (x + 1) + (z + 1) * WIDTH;
		}
	}



}

void TestApp::generateGrass(const std::vector<Vertex>& groundVertices) {

	grassLocations_.resize(WIDTH * HEIGHT);

	for (int x = 0; x < WIDTH; ++x) {
		for (int z = 0; z < HEIGHT; ++z) {

			if (rand() % 2) {
				glm::vec3 basePos = groundVertices[x * HEIGHT + z].pos;
				grassLocations_[x * HEIGHT + z] = { true, basePos };
			}
			else {
				grassLocations_[x * HEIGHT + z] = { false, glm::vec3() };
			}

		}
	}

}

void TestApp::init() {


	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Ground setup

	std::vector<Vertex> groundVertices_;
	std::vector<GLushort> groundIndices_;

	getGroundVertices(groundVertices_);
	getGroundIndices(groundIndices_);
	
	//Generate grass
	generateGrass(groundVertices_);

	groundvbo_.bufferData(groundVertices_);
	groundib_.bufferData(groundIndices_);

	groundvao_.addAttribute(groundvbo_, 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	groundvao_.addAttribute(groundvbo_, 1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)sizeof(glm::vec3));
	groundvao_.addAttribute(groundvbo_, 2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(2 * sizeof(glm::vec3)));

	groundTexture_.loadFromFile("ground.bmp");

	groundvs_.loadFromFile("groundShader.vert");
	groundfs_.loadFromFile("groundShader.frag");
	groundshader_.attachShader(groundvs_);
	groundshader_.attachShader(groundfs_);
	groundshader_.link();
	groundshader_.bindAttribLocation(0, "vs_in_pos");
	groundshader_.bindAttribLocation(1, "vs_in_norm");
	groundshader_.bindAttribLocation(2, "vs_in_tex");

	//Billboard setup
	std::vector<Vertex> squareVertices = {
		{glm::vec3(-0.5f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(0.0f, 0.0f)},
		{glm::vec3(+0.5f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1.0f, 0.0f)},
		{glm::vec3(+0.5f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(1.0f, 1.0f)},
		{glm::vec3(-0.5f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec2(0.0f, 1.0f)}
	};

	std::vector<GLushort> squareIndices = {
		0, 1, 3,
		1, 2, 3
	};

	billboardvbo_.bufferData(squareVertices);
	billboardib_.bufferData(squareIndices);
	billboardvao_.addAttribute(billboardvbo_, 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	billboardvao_.addAttribute(billboardvbo_, 1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)sizeof(glm::vec3));
	//billboardvao_.addAttribute(billboardvbo_, 2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(2 * sizeof(glm::vec3)));
	
	if (!billboardTexture_.loadFromFile("grass1.bmp")) {
		std::cerr << "OOpsie woopsie" << std::endl;
	}

	billboardvs_.loadFromFile("billboardShader.vert");
	billboardfs_.loadFromFile("billboardShader.frag");

	billboardshader_.attachShader(billboardvs_);
	billboardshader_.attachShader(billboardfs_);
	billboardshader_.link();
	billboardshader_.bindAttribLocation(0, "vs_in_pos");
	billboardshader_.bindAttribLocation(1, "vs_in_norm");
	//billboardshader_.bindAttribLocation(2, "vs_in_tex");

	passthroughvs_.loadFromFile("passthroughShader.vert");
	passthroughfs_.loadFromFile("passthroughShader.frag");

	passthroughshader_.attachShader(passthroughvs_);
	passthroughshader_.attachShader(passthroughfs_);
	passthroughshader_.link();
	passthroughshader_.bindAttribLocation(0, "vs_in_pos");
	passthroughshader_.bindAttribLocation(1, "vs_in_norm");
	passthroughshader_.bindAttribLocation(2, "vs_in_tex");

	cameraPos = glm::vec3(-5.0f, 5.0f, -5.0f);
}

void TestApp::processEvent(SDL_Event& event) {

}

void TestApp::update(Uint32 ms) {
	lightAngle += 0.0005f * ms;
	lightDir = glm::vec3(std::cosf(lightAngle), std::sinf(lightAngle), 0.0f);
}

void TestApp::draw() {
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 modelIT = glm::transpose(glm::inverse(model));
	glm::mat4 view = glm::lookAt(cameraPos, glm::vec3(3.0f, 0.0f, 3.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 proj = glm::perspective(45.0f, 640.0f / 480.0f, 1.0f, 100.0f);

	glm::mat4 MVP = proj * view * model;

	groundshader_.use();
	groundvao_.bind();
	groundib_.bind();
	groundTexture_.bind();


	groundshader_.setUniform1i("groundTexture", 0);

	groundshader_.setUniformMat4("MVP", &(MVP[0][0]));
	groundshader_.setUniformMat4("worldIT", &(modelIT[0][0]));
	groundshader_.setUniform3f("lightDir", lightDir.x, lightDir.y, lightDir.z);

	glDrawElements(GL_TRIANGLES, (WIDTH - 1) * (HEIGHT - 1) * 6, GL_UNSIGNED_SHORT, (void*)0);

	//----------------------------

	billboardshader_.use();
	billboardvao_.bind();
	billboardib_.bind();
	billboardTexture_.bind();

	billboardshader_.setUniform1i("grass_texture", 0);
	glm::mat4 VP = proj * view;
	billboardshader_.setUniformMat4("VP", &(VP[0][0]));
	billboardshader_.setUniform3f("cam_right", view[0][0], view[1][0], view[2][0]);
	billboardshader_.setUniform3f("cam_up", view[0][1], view[1][1], view[2][1]);

	for (int x = 0; x < WIDTH; ++x) {
		for (int z = 0; z < HEIGHT; ++z) {
			if (grassLocations_[x * HEIGHT + z].first) {
				glm::vec3& loc = grassLocations_[x * HEIGHT + z].second;
				billboardshader_.setUniform3f("billboard_pos", loc.x, loc.y, loc.z);

				glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, (void*)0);
			}
		}
	}
}
