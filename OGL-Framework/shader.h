#pragma once

#include <fstream>
#include <iostream>
#include <string>

#include <GL/glew.h>

namespace gl {

	enum ShaderType {
		Vertex = GL_VERTEX_SHADER,
		Fragment = GL_FRAGMENT_SHADER
	};

	template<ShaderType Type>
	class Shader {
	public:
		Shader();
		Shader(const Shader&) = delete;
		Shader& operator=(const Shader&) = delete;

		bool loadFromFile(const std::string& filename);

		operator GLuint() const;

		~Shader();
	private:
		GLuint id_ = 0;

		void create();
		void clean();
	};

	using VertexShader = Shader<Vertex>;
	using FragmentShader = Shader<Fragment>;
	
	//------------------------------------------------

	template<ShaderType Type>
	inline Shader<Type>::Shader() {
		create();
	}

	template<ShaderType Type>
	inline bool Shader<Type>::loadFromFile(const std::string& filename) {
		std::ifstream filestream(filename);
		if (!filestream.is_open()) {
			return false;
		}

		std::string code = "";
		std::string line = "";

		while (std::getline(filestream, line)) {
			code += line + "\n";
		}

		filestream.close();

		const char* strptr = code.c_str();
		glShaderSource(id_, 1, &strptr, nullptr);

		glCompileShader(id_);

		GLint result = GL_FALSE;
		GLint errorLength;

		glGetShaderiv(id_, GL_COMPILE_STATUS, &result);
		glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &errorLength);

		if (result != GL_TRUE) {
			GLchar* errormsg = new GLchar[errorLength];
			glGetShaderInfoLog(id_, errorLength, nullptr, errormsg);
			std::cerr << "Shader error: " << errormsg << std::endl;

			delete[] errormsg;

			return false;
		}

		return true;
	}

	template<ShaderType Type>
	inline Shader<Type>::operator GLuint() const {
		return id_;
	}

	template<ShaderType Type>
	inline Shader<Type>::~Shader() {
		clean();
	}

	template<ShaderType Type>
	inline void gl::Shader<Type>::create() {
		id_ = glCreateShader(static_cast<GLenum>(Type));
	}

	template<ShaderType Type>
	inline void gl::Shader<Type>::clean() {
		if (id_ != 0) {
			glDeleteShader(id_);
		}
	}
}
