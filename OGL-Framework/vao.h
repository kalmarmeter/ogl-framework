#pragma once

#include <GL/glew.h>

#include "buffer.h"

namespace gl {

	class VAO {
	public:
		VAO();
		VAO(const VAO&) = delete;
		VAO& operator=(const VAO&) = delete;

		void addAttribute(VBO& vbo, GLuint index, GLint count, GLenum type, GLboolean normalized, GLsizei stride, const void* ptr);
		//void addIndices();
		void bind();
		void unbind();

		~VAO();
	private:
		GLuint id_ = 0;

		void create();
		void clean();
	};

}