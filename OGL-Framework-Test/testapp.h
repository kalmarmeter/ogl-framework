#pragma once

#include "FastNoise.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <app.h>
#include <buffer.h>
#include <program.h>
#include <shader.h>
#include <texture.h>
#include <vao.h>

class TestApp : public App {
public:
	void init() override;
	void processEvent(SDL_Event& event) override;
	void update(Uint32 ms) override;
	void draw() override;
private:
	gl::VBO groundvbo_;
	gl::IB groundib_;
	gl::VAO groundvao_;
	gl::Texture groundTexture_;
	gl::VertexShader groundvs_;
	gl::FragmentShader groundfs_;
	gl::Program groundshader_;

	gl::VBO billboardvbo_;
	gl::IB billboardib_;
	gl::VAO billboardvao_;
	gl::Texture billboardTexture_;
	gl::VertexShader billboardvs_;
	gl::FragmentShader billboardfs_;
	gl::Program billboardshader_;

	gl::VertexShader passthroughvs_;
	gl::FragmentShader passthroughfs_;
	gl::Program passthroughshader_;

	struct Vertex {
		glm::vec3 pos;
		glm::vec3 normal;
		glm::vec2 tex;
	};

	const unsigned WIDTH = 100;
	const unsigned HEIGHT = 100;
	FastNoise noise_;

	void getGroundVertices(std::vector<Vertex>& groundVertices_);
	void getGroundIndices(std::vector<GLushort>& groundIndices_);
	void generateGrass(const std::vector<Vertex>& groundVertices);

	glm::vec3 cameraPos;
	float lightAngle = -M_PI;
	glm::vec3 lightDir;

	std::vector<std::pair<bool, glm::vec3>> grassLocations_;
};

