#version 440 core

in vec3 vs_in_pos;

out vec3 vs_out_pos;
out vec2 vs_out_tex;

void main() {
	gl_Position = vec4(vs_in_pos, 1);
	vs_out_pos = vs_in_pos;
	vs_out_tex = vs_out_pos.xy / 2.0 + vec2(0.5, 0.5);
}