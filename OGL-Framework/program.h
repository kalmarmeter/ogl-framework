#pragma once

#include <map>
#include <vector>

#include <GL/glew.h>

#include "shader.h"

namespace gl {

	class Program {
	public:
		Program();
		Program(const VertexShader& vs, const FragmentShader& fs);
		Program(const Program&) = delete;
		Program& operator=(const Program&) = delete;
		~Program();

		template<ShaderType Type>
		void attachShader(const Shader<Type>& shader);

		void bindAttribLocation(GLuint index, const std::string& name);
		bool link();
		void use();

		void setUniform1i(const std::string& name, GLint value);
		void setUniform2i(const std::string& name, GLint value1, GLint value2);
		void setUniform3i(const std::string& name, GLint value1, GLint value2, GLint value3);
		void setUniform4i(const std::string& name, GLint value1, GLint value2, GLint value3, GLint value4);
		void setUniform1f(const std::string& name, GLfloat value);
		void setUniform2f(const std::string& name, GLfloat value1, GLfloat value2);
		void setUniform3f(const std::string& name, GLfloat value1, GLfloat value2, GLfloat value3);
		void setUniform4f(const std::string& name, GLfloat value1, GLfloat value2, GLfloat value3, GLfloat value4);
		void setUniformMat2(const std::string& name, const GLfloat* matrix);
		void setUniformMat3(const std::string& name, const GLfloat* matrix);
		void setUniformMat4(const std::string& name, const GLfloat* matrix);
	private:
		GLuint id_ = 0;
		std::vector<GLuint> attachedShaders_;
		std::map<std::string, GLuint> uniformLocations_;

		void create();
		void clean();
		GLuint findUniformLocation(const std::string& name);

	};



	template<ShaderType Type>
	inline void Program::attachShader(const Shader<Type>& shader) {
		glAttachShader(id_, shader);
		attachedShaders_.push_back(shader);
	}

}