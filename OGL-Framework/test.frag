#version 440 core

in vec3 vs_out_pos;
in vec2 vs_out_tex;

out vec4 fs_out_col;

uniform sampler2D sampler;

void main() {
	fs_out_col = vec4(texture(sampler, vs_out_tex));
}